import React from "react";
import {translations} from "@/translations";
import {TechnologiesBlock} from "@/components/TechnologiesBlock";
import {LangsE} from "@/types/langsTypes";
import {RepoBlock} from "@/components/RepoBlock";
import {LinksBlock} from "@/components/LinksBlock";
import {CarouselItemLayout} from "@/components/WorksCarousel/components/CarouselItemLayout";
import {ReactIcon} from "@/components/icons/ReactIcon";
import {PostgresIcon} from "@/components/icons/PostgresIcon";
import {RedisIcon} from "@/components/icons/RedisIcon";
import {SentryIcon} from "@/components/icons/SentryIcon";
import {DockerIcon} from "@/components/icons/DockerIcon";
import {OAuthIcon} from "@/components/icons/OAuthIcon";
import styles from './styles.module.css'

type SeaBattlesWebProps = {
    lang: LangsE;
}

const technologies = [
    {text: 'React', icon: <ReactIcon/>},
    'ExpressJS',
    'WebSockets',
    {text: 'PostgreSQL', icon: <PostgresIcon/>},
    {text: 'Redis', icon: <RedisIcon/>},
    {text: 'Sentry', icon: <SentryIcon/>},
    {text: 'OAuth 2.0', icon: <OAuthIcon/>},
    {text: 'Docker', icon: <DockerIcon/>}
]

export const SeaBattlesWeb: React.FC<SeaBattlesWebProps> = ({lang}) => {
    const title = translations[lang].works.seaBattles.title
    const description = translations[lang].works.seaBattles.description
    return <CarouselItemLayout
        title={title}
        description={description}
        backgroundBlock={<div className={styles.background}/>}
    >
        <TechnologiesBlock technologies={technologies} lang={lang}/>
        <RepoBlock repos={[
            {label: 'Frontend', link: 'https://gitlab.com/sea-battle1/sea-battle-client'},
            {label: 'Backend', link: 'https://gitlab.com/sea-battle1/sea-battle-back'}
        ]} lang={lang}
        />
        <LinksBlock links={[
            {label: 'Web-site', link: 'https://sea-battles.ru/'},
            {label: 'Telegram Game', link: 'https://t.me/@SeaBattlesBot'},
            {label: 'VK game', link: 'https://vk.com/app51517697_19657558'},
        ]} lang={lang}/>
    </CarouselItemLayout>
}