import React from "react";
import {translations} from "@/translations";
import {TechnologiesBlock} from "@/components/TechnologiesBlock";
import {LangsE} from "@/types/langsTypes";
import {RepoBlock} from "@/components/RepoBlock";
import {LinksBlock} from "@/components/LinksBlock";
import {CarouselItemLayout} from "@/components/WorksCarousel/components/CarouselItemLayout";
import {PostgresIcon} from "@/components/icons/PostgresIcon";
import {DockerIcon} from "@/components/icons/DockerIcon";
import {NestIcon} from "@/components/icons/NestIcon";
import {NextJSIcon} from "@/components/icons/NextJSIcon";
import {NginxIcon} from "@/components/icons/NginxIcon";
import {GitLabIcon} from "@/components/icons/GitLabIcon";
import styles from './styles.module.css'

type CustomVideoHostingProps = {
    lang: LangsE;
}

const technologies = [
    {text: 'NextJS', icon: <NextJSIcon/>},
    {text: 'NestJS', icon: <NestIcon/>},
    {text: 'PostgresSQL', icon: <PostgresIcon/>},
    {text: 'NGINX', icon: <NginxIcon/>},
    "CI/CD",
    {text: 'Gitlab-runner', icon: <GitLabIcon/>},
    "Custom authorization",
    "ffmpeg",
    {text: 'Docker', icon: <DockerIcon/>}
]

export const CustomVideoHosting: React.FC<CustomVideoHostingProps> = ({lang}) => {
    const title = translations[lang].works.videoHosting.title
    const description = translations[lang].works.videoHosting.description
    return <CarouselItemLayout
        title={title}
        description={description}
        backgroundBlock={<div className={styles.background}/>}
    >
        <TechnologiesBlock technologies={technologies} lang={lang}/>
        <RepoBlock repos={[
            {label: 'Frontend', link: 'https://gitlab.com/video-hosting/video-hosting-frontend'},
            {label: 'Backend', link: 'https://gitlab.com/video-hosting/video-hosting-backend'}
        ]} lang={lang}
        />
        <LinksBlock links={[
            {label: 'Web-site', link: 'https://video-hosting.site/'},
        ]} lang={lang}/>
    </CarouselItemLayout>
}