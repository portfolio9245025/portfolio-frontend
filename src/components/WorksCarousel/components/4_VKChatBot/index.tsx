import React from "react";
import {translations} from "@/translations";
import {TechnologiesBlock} from "@/components/TechnologiesBlock";
import {LangsE} from "@/types/langsTypes";
import {RepoBlock} from "@/components/RepoBlock";
import {LinksBlock} from "@/components/LinksBlock";
import {CarouselItemLayout} from "@/components/WorksCarousel/components/CarouselItemLayout";
import {JSIcon} from "@/components/icons/JSIcon";
import {VKIcon} from "@/components/icons/VKIcon";
import styles from './styles.module.css'

type VKChatBotProps = {
    lang: LangsE;
}

const technologies = [
    {text: 'JavaScript', icon: <JSIcon />},
    'ExpressJS',
    {text: 'VK API', icon: <VKIcon />},
]

export const VKChatBot: React.FC<VKChatBotProps> = ({lang}) => {
    const title = translations[lang].works.chatBotForVK.title
    const description = translations[lang].works.chatBotForVK.description
    const VKChatBotGroup = translations[lang].works.chatBotForVK.VKChatBotGroup
    return <CarouselItemLayout
        title={title}
        description={description}
        backgroundBlock={<div className={styles.background}/>}
    >
        <TechnologiesBlock
            technologies={technologies}
            lang={lang}
        />
        <RepoBlock repos={[
            {label: 'Backend', link: 'https://github.com/Fristail27/BOT-VK'},
        ]} lang={lang}
        />
        <LinksBlock links={[
            {label: VKChatBotGroup, link: 'https://vk.com/public205979130'},
        ]} lang={lang}/>
    </CarouselItemLayout>
}