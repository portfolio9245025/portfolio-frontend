import React from "react";
import {TitleBlock} from "@/components/TitleBlock";
import {LangsE} from "@/types/langsTypes";
import styles from './styles.module.css'

type CarouselItemLayoutProps = {
    children: React.ReactNode;
    backgroundBlock: React.ReactNode;
    title: string;
    description: string;
}

export const CarouselItemLayout: React.FC<CarouselItemLayoutProps> = ({
                                                                          children,
                                                                          title,
                                                                          description,
                                                                          backgroundBlock
                                                                      }) => {
    return <div className={styles.container}>
        {backgroundBlock}
        <TitleBlock title={title} h={3}/>
        <p className={styles.description}><mark className={styles.markColor}>{description}</mark></p>
        <div className={styles.rowBlocks}>
            {children}
        </div>
    </div>
}