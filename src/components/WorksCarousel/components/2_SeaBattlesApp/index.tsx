import React from "react";
import Image from 'next/image'
import {translations} from "@/translations";
import {TechnologiesBlock} from "@/components/TechnologiesBlock";
import {LangsE} from "@/types/langsTypes";
import {RepoBlock} from "@/components/RepoBlock";
import {LinksBlock} from "@/components/LinksBlock";
import {CarouselItemLayout} from "@/components/WorksCarousel/components/CarouselItemLayout";
import styles from './styles.module.css'
import {ReactIcon} from "@/components/icons/ReactIcon";
import {PostgresIcon} from "@/components/icons/PostgresIcon";
import {RedisIcon} from "@/components/icons/RedisIcon";
import {SentryIcon} from "@/components/icons/SentryIcon";
import {OAuthIcon} from "@/components/icons/OAuthIcon";
import {DockerIcon} from "@/components/icons/DockerIcon";
import {InternationalizationIcon} from "@/components/icons/InternationalizationIcon";

type SeaBattlesAppProps = {
    lang: LangsE;
}

const technologies = [
    {text: 'React', icon: <ReactIcon/>},
    {text: 'React Native', icon: <ReactIcon/>},
    {text: 'Internationalization i18tn', icon: <InternationalizationIcon/>},
    'ExpressJS',
    'WebSockets',
    {text: 'PostgreSQL', icon: <PostgresIcon/>},
    {text: 'Redis', icon: <RedisIcon/>},
    {text: 'Sentry', icon: <SentryIcon/>},
    {text: 'OAuth 2.0', icon: <OAuthIcon/>},
    {text: 'Docker', icon: <DockerIcon/>}
]

export const SeaBattlesApp: React.FC<SeaBattlesAppProps> = ({lang}) => {
    const title = translations[lang].works.seaBattlesApp.title
    const description = translations[lang].works.seaBattlesApp.description
    return <CarouselItemLayout
        title={title}
        description={description}
        backgroundBlock={<>
            <div className={styles.appExample}/>
            <div className={styles.background}/>
        </>}>
        <TechnologiesBlock
            technologies={technologies}
            lang={lang}
        />
        <RepoBlock repos={[
            {label: 'Frontend App', link: 'https://gitlab.com/sea-battle1/sea-battles-native'},
            {label: 'Backend', link: 'https://gitlab.com/sea-battle1/sea-battle-back'}
        ]} lang={lang}
        />
        <LinksBlock links={[
            {
                label: 'IOS',
                link: 'https://apps.apple.com/ru/app/%D0%BC%D0%BE%D1%80%D1%81%D0%BA%D0%BE%D0%B9-%D0%B1%D0%BE%D0%B9-battleships/id6446815384'
            },
            {label: 'Android', link: 'https://apps.rustore.ru/app/com.fristail27.seabattlesmobile'},
        ]} lang={lang}/>
    </CarouselItemLayout>
}