import React from "react";
import {translations} from "@/translations";
import {TechnologiesBlock} from "@/components/TechnologiesBlock";
import {LangsE} from "@/types/langsTypes";
import {RepoBlock} from "@/components/RepoBlock";
import {LinksBlock} from "@/components/LinksBlock";
import {CarouselItemLayout} from "@/components/WorksCarousel/components/CarouselItemLayout";
import {OAuthIcon} from "@/components/icons/OAuthIcon";
import {SQLiteIcon} from "@/components/icons/SQLiteIcon";
import {JSIcon} from "@/components/icons/JSIcon";
import styles from './styles.module.css'

type CustomOAuthProps = {
    lang: LangsE;
}

const technologies = [
    {text: 'JavaScript', icon: <JSIcon />},
    'ExpressJS',
    {text: 'SQLite', icon: <SQLiteIcon />},
    {text: 'OAuth 2.0', icon: <OAuthIcon />},
]

export const CustomOAuth: React.FC<CustomOAuthProps> = ({lang}) => {
    const title = translations[lang].works.customOAuth.title
    const description = translations[lang].works.customOAuth.description
    const notLinksMessage = translations[lang].works.notLinksMessage
    const exampleForClient = translations[lang].works.exampleForClient
    return <CarouselItemLayout
        title={title}
        description={description}
        backgroundBlock={<div className={styles.background}/>}
    >
        <TechnologiesBlock
            technologies={technologies}
            lang={lang}
        />
        <RepoBlock repos={[
            {label: 'Backend', link: 'https://gitlab.com/nexia-a6-pro/na6-backend/-/tree/develop?ref_type=heads'},
            {
                label: exampleForClient,
                link: 'https://gitlab.com/nexia-a6-pro/na6-frontend/-/tree/develop?ref_type=heads'
            },
        ]} lang={lang}
        />
        <LinksBlock links={[]} text={notLinksMessage} lang={lang}/>
    </CarouselItemLayout>
}