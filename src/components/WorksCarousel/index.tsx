'use client'
import React from "react";
import {SeaBattlesWeb} from "@/components/WorksCarousel/components/1_SeaBattlesWeb";
import {Carousel} from "antd";
import {SeaBattlesApp} from "@/components/WorksCarousel/components/2_SeaBattlesApp";
import {CustomOAuth} from "@/components/WorksCarousel/components/3_CustomOAuth";
import {VKChatBot} from "@/components/WorksCarousel/components/4_VKChatBot";
import {LangsE} from "@/types/langsTypes";
import styles from "./styles.module.css";
import {CustomVideoHosting} from "@/components/WorksCarousel/components/5_CustomVideoHosting";

type WorksCarouselChildrenP = {
    lang: LangsE
}

export const WorksCarouselChildren:React.FC<WorksCarouselChildrenP> = ({lang}) => {
    return <Carousel
        rootClassName={styles.carouselRoot}
        className={styles.carousel}
        autoplay
        autoplaySpeed={15000}
    >
        <CustomVideoHosting lang={lang}/>
        <SeaBattlesWeb lang={lang}/>
        <SeaBattlesApp lang={lang}/>
        <CustomOAuth lang={lang}/>
        <VKChatBot lang={lang}/>
    </Carousel>
}