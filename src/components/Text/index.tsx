import React from "react";
import styles from './styles.module.css'

type TextProps = {
    text: string;
    weight?: number;
}

export const Text:React.FC<TextProps> = ({text, weight}) => {
    return <span style={{fontWeight: weight}} className={styles.text}>{text}</span>
}