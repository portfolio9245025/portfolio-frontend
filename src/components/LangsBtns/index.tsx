'use client'
import styles from './styles.module.css'
import Link from "next/link";
import {useParams, usePathname} from "next/navigation";

export const LangsBtns = () => {
    const pathName = usePathname()
    const params = useParams()
    const lang = params.lang
    return <aside className={styles.langsBtnsContainer}>
        <Link className={`${styles.langBtn} ${lang === 'ru' ? styles.isNotActive : ''}`} href={`${pathName.replace('ru', 'en')}`}>EN</Link>
        <Link className={`${styles.langBtn} ${lang === 'en' ? styles.isNotActive : ''}`} href={`${pathName.replace('en', 'ru')}`}>RU</Link>
    </aside>
}
