import React from "react";
import {translations} from "@/translations";
import {LangsE} from "@/types/langsTypes";
import {TitleBlock} from "@/components/TitleBlock";
import {Text} from "@/components/Text";
import styles from './styles.module.css'

type LinkT = {
    label: string;
    link: string;
}

type LinksBlockT = {
    links: LinkT[];
    lang: LangsE;
    text?: string;
}

export const LinksBlock: React.FC<LinksBlockT> = ({lang, links, text}) => {
    const title = translations[lang].works.links
    return <div className={styles.linksBlock}>
        <TitleBlock title={title}/>
        <span className={styles.linksSpanWrapper}>
            {links.map(r => <mark
                key={r.label}
                className={styles.repoRow}
            ><Text weight={700} text={`${r.label}: `}/>
                <a target="_blank" className={styles.repoLink} href={r.link}>
                    <Text text={r.link}/>
                </a>
            </mark>)}
            {text && <mark className={styles.markColor}><Text text={text}/></mark>}
        </span>
    </div>
}