import React from 'react'
import {translations} from "@/translations";
import {LangsE} from "@/types/langsTypes";
import styles from './styles.module.css'
import {TitleBlock} from "@/components/TitleBlock";
import {Text} from "@/components/Text";

type RepoT = {
    label: string;
    link: string;
}

type RepoBlockProps = {
    lang: LangsE;
    repos: RepoT[];
}

export const RepoBlock:React.FC<RepoBlockProps> = ({lang, repos}) => {
    const title = translations[lang].works.repositories
    return <div className={styles.repoBlock}>
        <TitleBlock title={title}/>
        <span className={styles.linksSpanWrapper}>
           {repos.map(r => <mark
               key={r.label}
               className={styles.repoRow}
           >
               <Text weight={700} text={`${r.label}: `}/>
               <a target="_blank" className={styles.repoLink} href={r.link}>
                   <Text text={r.link}/>
               </a>
           </mark>)}
        </span>
    </div>
}