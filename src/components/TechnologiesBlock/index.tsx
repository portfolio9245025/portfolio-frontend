import React from "react";
import {translations} from "@/translations";
import {LangsE} from "@/types/langsTypes";
import {TitleBlock} from "@/components/TitleBlock";
import {Text} from "@/components/Text";
import styles from './styles.module.css'

type TechnologyItemT = {
    text: string;
    icon: React.ReactNode
} | string

type TechnologiesBlockT = {
    technologies: TechnologyItemT[];
    lang: LangsE
}

export const TechnologiesBlock: React.FC<TechnologiesBlockT> = ({lang, technologies}) => {
    const title = translations[lang].works.technologies
    return <div className={styles.technologiesBlock}>
        <TitleBlock title={title}/>
        <span className={styles.valuesContainer}>
        {technologies.map((el, i) => {
            if (typeof el === 'string') {
                const text = technologies.length - 1 === i ? el : `${el}, `
                return <mark className={styles.markColor} key={el}><Text text={text}/></mark>
            }
            const text = technologies.length - 1 === i ? el.text : `${el.text}, `

            return <mark className={styles.spanWithIcon} key={text}>
                {el.icon}
                <Text text={text}/>
            </mark>
        })}
            </span>
    </div>
}