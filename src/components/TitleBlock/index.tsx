import React from "react";
import styles from './styles.module.css'

type TitleBlockProps = {
    title: string;
    h?: number;
}

export const TitleBlock:React.FC<TitleBlockProps> = ({title, h}) => {
    if (h === 2) {
        return <h2 className={styles.titleH2}>{title}:</h2>
    }
    if (h === 3) {
        return <h3 className={styles.titleH3}><mark className={styles.markColor}>{title}:</mark></h3>
    }
    return <h5 className={styles.title}><mark className={styles.markColor}>{title}:</mark></h5>
}