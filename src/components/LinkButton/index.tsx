'use client'
import React from 'react'
import Link from "next/link"
import styles from './styles.module.css'
import {useRouter} from "next/navigation";
import {ArrowRightIcon} from "@/components/icons/ArrowRightIcon";

type LinkButtonT = {
    text: string;
    href: string;
}

export const LinkButton: React.FC<LinkButtonT> = ({text, href}) => {
    const router = useRouter()
    return <a className={styles.linkButton} onClick={(e) => {
        const mainBlock = document.getElementById('main')
        mainBlock.classList.add(styles.hiddenMainBlock)
        setTimeout(() => {
            router.push(href)
        }, 250)
    }} >
        {text}
        <ArrowRightIcon className={styles.arrowRight}/>
    </a>
}