import {translations} from "@/translations";
import {WorksCarouselChildren} from "@/components/WorksCarousel";
import {LinkButton} from "@/components/LinkButton";
import {TitleBlock} from "@/components/TitleBlock";
import {Metadata} from "next";
import styles from './page.module.css'

export const metadata: Metadata = {
    title: 'Alexey Ryzhov | My works',
}

export default function MyWorksPage({params}) {
    const title = translations[params.lang].myWorks
    const contacts = translations[params.lang].contacts
    return (
        <main id="main" className={styles.contactsBlock}>
            <div className={styles.toContactsLink}>
                <LinkButton text={contacts} href={`/${params.lang}/contacts`}/>
            </div>
            <section className={styles.content}>
                <TitleBlock title={title} h={2}/>
                <WorksCarouselChildren lang={params.lang}/>
            </section>
        </main>
    )
}
