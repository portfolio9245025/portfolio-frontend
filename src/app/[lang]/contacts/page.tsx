import Link from "next/link";
import {Metadata} from "next";
import {translations} from "@/translations";
import {TGIcon} from "@/components/icons/TGIcon";
import {GitLabIcon} from "@/components/icons/GitLabIcon";
import {GitHubIcon} from "@/components/icons/GitHubIcon";
import {LinkButton} from "@/components/LinkButton";
import {TitleBlock} from "@/components/TitleBlock";
import {ResumeIcon} from "@/components/icons/ResumeIcon";
import styles from './page.module.css'
import {LangsE} from "@/types/langsTypes";

type ContactsPageP = {
    params: {
        lang: LangsE
    }
}

export const metadata: Metadata = {
    title: 'Alexey Ryzhov | Contacts',
}

export default function ContactsPage({params}: ContactsPageP) {
    const contactTitle = translations[params.lang].contacts
    const aboutMeTitle = translations[params.lang].aboutMe
    const sendTG = translations[params.lang].sendTG
    const myWorks = translations[params.lang].myWorks
    const resume = translations[params.lang].resume
    return (
        <main id="main" className={styles.contactsBlock}>
            <section className={styles.content}>
                <TitleBlock title={contactTitle} h={2}/>
                <div className={styles.tgRow}>
                    <TGIcon/>
                    <a target="_blank" className={styles.tgLink} href="https://t.me/ololo434">{sendTG}</a>
                </div>
            </section>
            <section className={styles.content}>
                <TitleBlock title={aboutMeTitle} h={2}/>
                <div className={styles.tgRow}>
                    <ResumeIcon/>
                     <Link
                         target="_blank"
                         className={styles.tgLink}
                         href="/AlexeyRyzhov-Resume.pdf"
                     >
                         {resume}
                     </Link>
                </div>
                <div className={styles.tgRow}>
                    <GitLabIcon/>
                    <a target="_blank" className={styles.tgLink} href="https://gitlab.com/fristail27">GitLab</a>
                </div>
                <div className={styles.tgRow}>
                    <GitHubIcon/>
                    <a target="_blank" className={styles.tgLink} href="https://github.com/Fristail27">GitHub</a>
                </div>
            </section>
            <LinkButton text={myWorks} href={`/${params.lang}/my-works`}/>
        </main>
    )
}
