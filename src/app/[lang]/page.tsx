import {Metadata} from "next";
import {redirect} from 'next/navigation';
import {translations} from "@/translations";
import {LinkButton} from "@/components/LinkButton";
import styles from './page.module.css'
import {LangsE} from "@/types/langsTypes";

type HomePageP = {
    params: {
        lang: LangsE
    }
}

export const generateMetadata = async ({ params }: HomePageP):Promise<Metadata> => {
    if (params.lang === 'ru') {
        return {
            title: 'Алексей Рыжов',
            description: 'Alexey Ryzhov personal portfolio\'s website.',
            openGraph: {
                title: 'Алексей Рыжов',
                description: 'Alexey Ryzhov personal portfolio\'s website.',
                type: 'website',
            }
        }
    }
    return {
        title: 'Alexey Ryzhov',
        description: 'Alexey Ryzhov personal portfolio\'s website.',
        openGraph: {
            title: 'Alexey Ryzhov',
            description: 'Alexey Ryzhov personal portfolio\'s website.',
            type: 'website',
        }
    }
}

export default function Home({params}: HomePageP) {
    if (params.lang !== 'en' && params.lang !== 'ru') {
        redirect('/en');
    }
    const fullName = translations[params.lang].fullName
    const position = translations[params.lang].position
    const myWorks = translations[params.lang].myWorks
    const contacts = translations[params.lang].contacts

    return (
        <main className={styles.main} id="main">
            <h1 className={styles.text_fade}>{fullName}</h1>
            <section className={styles.buttonsContainer}>
                <LinkButton text={myWorks} href={`/${params.lang}/my-works`}/>
                <LinkButton text={contacts} href={`/${params.lang}/contacts`}/>
            </section>
            <h2 className={styles.text_fade2}>{position}</h2>
        </main>
    )
}
