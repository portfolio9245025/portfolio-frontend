import Script from "next/script";
import localFont from '@next/font/local'
import {LangsBtns} from "@/components/LangsBtns";
import './globals.css'

const RobotoMono = localFont({
    src: [
        {
            path: '../../public/fonts/RobotoMono-Light.ttf',
            weight: '100',
        },
        {
            path: '../../public/fonts/RobotoMono-Regular.ttf',
            weight: '300',
        },
        {
            path: '../../public/fonts/RobotoMono-Bold.ttf',
            weight: '700',
        },
    ],
    variable: '--RobotoMono'
})

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <html lang="en" className={`${RobotoMono.variable} font-sans`}>
        <head>
            <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
            <link rel="manifest" href="/site.webmanifest"/>
            <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#787878"/>
            <meta name="msapplication-TileColor" content="#2b5797"/>
            <meta name="theme-color" content="#ffffff"/>
        </head>
        <body>
        <div className="animateBackground"/>
        <div className="animateBackground"/>
        <div className="animateBackground"/>
        <Script src="/createjs.min.js"/>
        <Script src="/gsap.min.js"/>
        <Script src='/script.js'/>
        <LangsBtns/>
        {children}
        </body>
        </html>
    )
}
