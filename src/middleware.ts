import { NextRequest, NextResponse } from 'next/server'

export function middleware(request: NextRequest) {
    const pathname = request.nextUrl.pathname
    if (pathname === '/') {
        return NextResponse.redirect(new URL('/en', request.url))
    }
    const [_, lang, contentPage, other] = pathname.split('/')

    if (lang !== 'ru' && lang !== 'en') {
        return NextResponse.redirect(new URL('/en', request.url))
    }

    if (contentPage && contentPage !== 'my-works' && contentPage !== 'contacts') {
        return NextResponse.redirect(new URL(`/${lang}`, request.url))
    }

    if (other) {
        return NextResponse.redirect(new URL(`/${lang}/${contentPage}`, request.url))
    }
}
export const config = {
    matcher: ['/((?!api|_next/static|_next/image|_next/favicon.ico|script.js|createjs.min.js|gsap.min.js|animate.js|AlexeyRyzhov-Resume.pdf|favicon-16x16.png|favicon-32x32.png|mstile-150x150.png|safari-pinned-tab.svg|site.webmanifest).*)'],
}
