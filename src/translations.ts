export const translations = {
    en: {
        "fullName": "Alexey Ryzhov",
        "position": "Frontend-developer",
        myWorks: 'My works',
        contacts: 'Contacts',
        aboutMe: 'About me',
        resume: 'Resume',
        sendTG: 'Write to me in a Telegram',
        works: {
            technologies: 'Technologies',
            repositories: 'Repositories',
            links: 'Links',
            notLinksMessage: 'Not deployed. It is used to implement quick authorization when developing other projects.',
            exampleForClient: 'Example for use in client',
            seaBattles: {
                title: 'Sea-Battles web',
                description: 'My own project. Implementation of the classic sea battle game. ' +
                    "Single and multiplayer modes are implemented, as well as computer simulation of battles. " +
                    'The game is available as a separate resource, as well as on Telegram games and VK games platforms '
            },
            seaBattlesApp: {
                title: 'Sea-Battles App',
                description: 'My own project. Implementation of the classic sea battle game. ' +
                    "Single and multiplayer modes. Available for IOS and Android"
            },
            customOAuth: {
                title: 'Custom OAuth service',
                description: 'My own project. Custom OAuth service.'
            },
            chatBotForVK: {
                title: 'Chat bot for VK social network',
                description: 'My own project. Chat bot for VK social network. The bot\'s functionality includes various jokes for communicating in community chat.',
                VKChatBotGroup: 'VK chat bot group'
            },
            videoHosting: {
                title: 'Custom Video hosting',
                description: 'My own project. Custom video hosting service. It is available to add and view videos, comments, subscriptions, likes of liked videos, view counters.'
            }
        }
    },
    ru: {
        "fullName": "Алексей Рыжов",
        "position": "Frontend-developer",
        myWorks: 'Мои работы',
        contacts: 'Контакты',
        aboutMe: 'Обо мне',
        sendTG: 'Связаться со мной в Телеграмме',
        resume: 'Резюме',
        works: {
            technologies: 'Технологии',
            repositories: 'Репозитории',
            links: 'Ресурсы',
            notLinksMessage: 'Не размещено в открытом доступе. Используется для реализации быстрой авторизации при разработке других проектов.',
            exampleForClient: 'Пример использования в клиенте',
            seaBattles: {
                title: 'Морской бой веб-версия',
                description: 'Мой проект. Реализация игры в классический морской бой. ' +
                    'Реализованы одиночный и многопользовательский режимы, а так же симуляция боев компьютером. ' +
                    'Игра доступна как отдельный ресурс, а так же на платформах Telegram games и ВК игры'
            },
            seaBattlesApp: {
                title: 'Морской бой приложение',
                description: 'Мой проект. Реализация игры в классический морской бой. ' +
                    'Реализованы одиночный и многопользовательский режимы, а так же симуляция боев компьютером. ' +
                    'Игра доступна для IOS и Android'
            },
            customOAuth: {
                title: 'Кастомный OAuth сервис',
                description: 'Мой проект. Реализация кастомного OAuth сервиса.'
            },
            chatBotForVK: {
                title: 'Чат бот для ВК',
                description: 'Мой проект. Чат бот для социальной сети ВК. Функционал бота включает различные шутки для общения в чате сообществ.',
                VKChatBotGroup: 'Группа ВК'
            },
            videoHosting: {
                title: 'Кастомный видеохостинг',
                description: 'кастомный сервис видеохостинга. Доступно добавление и просмотры видео, комментарии, подписки, лайки понравившихся видео, счетчики просмотров.'
            }
        }
    }
}
