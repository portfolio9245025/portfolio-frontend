module.exports = {
    apps: [
        {
            name: "portfolio",
            script: "yarn start",
            args: "limit",
            instances: 1
        }
    ]
}